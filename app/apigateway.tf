resource "aws_api_gateway_rest_api" "this" {
  # body = jsonencode({
  #   openapi = "3.0.1"
  #   info = {
  #     title   = "example"
  #     version = "1.0"
  #   }
  #   paths = {
  #     "/path1" = {
  #       get = {
  #         x-amazon-apigateway-integration = {
  #           httpMethod           = "GET"
  #           payloadFormatVersion = "1.0"
  #           type                 = "HTTP_PROXY"
  #           uri                  = "https://ip-ranges.amazonaws.com/ip-ranges.json"
  #         }
  #       }
  #     }
  #   }
  # })

  name = var.name
  description = "Test"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = ""

  // Weird cyclic dependency issues - in this case, triggered due to change in parent_id
  // https://github.com/hashicorp/terraform-provider-aws/issues/11344
  lifecycle {
    ignore_changes = [ parent_id ]
  }
}

resource "aws_api_gateway_method" "this" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this.id
  http_method   = "GET"
  authorization = "NONE"

  api_key_required = true
}

resource "aws_lambda_permission" "this" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/*/*"
}

resource "aws_api_gateway_integration" "this" {
  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this.id
  http_method             = aws_api_gateway_method.this.http_method
  integration_http_method = "GET"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}

# TO FILL IN resources, including but not limited to
# - aws_api_gateway_rest_api
# - aws_api_gateway_method
# - aws_api_gateway_deployment

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.this.id,
      aws_api_gateway_method.this.id,
      aws_api_gateway_integration.this.id,
      aws_api_gateway_domain_name.this.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "this" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.this.id
  stage_name    = var.stage

  lifecycle {
    replace_triggered_by = [ aws_api_gateway_deployment.this ]
  }
}

resource "aws_api_gateway_api_key" "this" {
  name = "${var.name}-test"
}

resource "aws_api_gateway_usage_plan" "this" {
  name = var.name

  quota_settings {
    limit = 20
    period = "DAY"
  }

  throttle_settings {
    burst_limit = 5
    rate_limit = 25
  }

  api_stages {
    api_id = aws_api_gateway_rest_api.this.id
    stage  = aws_api_gateway_stage.this.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "main" {
  key_id        = aws_api_gateway_api_key.this.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.this.id
}

resource "aws_api_gateway_domain_name" "this" {
  domain_name     = var.api_domain_name
}